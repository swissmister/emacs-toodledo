;;; toodledo.el --- Toodledo API access and org-mode integration
;;; Commentary:

(require 'oauth2)
(require 'gh-url)  ;; TODO get rid of this dependency, it it  only used for parsing headers
;; basic - access to basic account information.
;; tasks - access to tasks.
;; notes - access to notes.
;; outlines - access to outlines.
;; lists - access to lists.
;; share - access to user's collaborator's information.

;;; Code:

(defgroup toodledo nil
  "Toodledo V3 API support in Emacs"
  :prefix "toodledo-")

(defcustom toodledo-client-id "EmacsToodledo"
  "UserID from Toodledo (not your e-mail address).
See http://www.toodledo.com/info/api_doc.php"
  :group 'toodledo
  :type 'string)

(defcustom toodledo-auth-url "https://api.toodledo.com/3/account/authorize.php"
  ""
  :group 'toodledo)

(defcustom toodledo-token-url  "https://api.toodledo.com/3/account/token.php"
  ""
  :group 'toodledo)

(defcustom toodledo-resource-url ""
  ""
  :group 'toodledo)

(defcustom toodledo-client-id "EmacsToodledo"
  ""
  :group 'toodledo)

(defcustom toodledo-client-secret nil
  ""
  :group 'toodledo)

(defvar resource-url nil)
(defvar redirect-uri "https://localhost:8090/index.html")

(defvar account-url "https://api.toodledo.com/3/account/get.php")



(defun toodledo-token () ""
       (toodledo-oauth2-auth-and-store
        toodledo-auth-url
        toodledo-token-url
        "basic tasks notes lists outlines share write"
        toodledo-client-id
        toodledo-client-secret
        toodledo-resource-url
        "state"))

;; Redefine function to manage state correctly

;;;###autoload
(defun toodledo-oauth2-auth-and-store (auth-url token-url resource-url client-id client-secret
                                                &optional redirect-uri state)
  "Request access to a resource and store it using `plstore'."
  ;; We store a MD5 sum of all URL
  (let* ((plstore (plstore-open oauth2-token-file))
         (id (oauth2-compute-id auth-url token-url resource-url))
         (plist (cdr (plstore-get plstore id))))
    ;; Check if we found something matching this access
    (if plist
        ;; We did, return the token object
        (make-oauth2-token :plstore plstore
                           :plstore-id id
                           :client-id client-id
                           :client-secret client-secret
                           :access-token (plist-get plist :access-token)
                           :refresh-token (plist-get plist :refresh-token)
                           :token-url token-url
                           :access-response (plist-get plist :access-response))
      (let ((token (oauth2-auth auth-url token-url
                                client-id client-secret resource-url state redirect-uri)))
        ;; Set the plstore
        (setf (oauth2-token-plstore token) plstore)
        (setf (oauth2-token-plstore-id token) id)
        (plstore-put plstore id nil `(:access-token
                                      ,(oauth2-token-access-token token)
                                      :refresh-token
                                      ,(oauth2-token-refresh-token token)
                                      :access-response
                                      ,(oauth2-token-access-response token)))
        (plstore-save plstore)
        token))))

(defun handle-api-response (status &optional cbargs)
  " "
  (let ((headers (gh-url-parse-headers
                  (buffer-substring
                   (point-min) (1+ url-http-end-of-headers)))))
    (debug headers)
    (goto-char url-http-end-of-headers)
    (let* ((json-object-type 'plist)
           (data (json-read)))
      (debug data))))

(defun toodledo-api-request (url &optional request-method request-data)
  ""
  (let* ((request-extra-headers (if (string= request-method "POST")
                                    '(("Content-Type" . "application/x-www-form-urlencoded"))))
         (response-buffer (oauth2-url-retrieve-synchronously
                           (toodledo-token)
                           url
                           request-method
                           request-data
                           request-extra-headers)))
    (with-current-buffer response-buffer
      ;; TODO check response for errors
      (goto-char url-http-end-of-headers)
      (let* ((json-object-type 'plist)
             (json-array-type 'list)
             (data (json-read)))
        ;; (switch-to-buffer-other-window response-buffer)
        data))))

;;; Accounts
(defun toodledo-account-info ()
  (toodledo-api-request account-url))

;;; Folders
(defun toodledo-folders-get ()
  (toodledo-api-request "http://api.toodledo.com/3/folders/get.php"))

(defun toodledo-folder-add (folder)
  (let ((url-request-data
         (mapconcat (lambda (arg)
                      (concat (url-hexify-string (car arg)) "=" (url-hexify-string (cdr arg))))
                    (list folder) "&")))

    (toodledo-api-request "http://api.toodledo.com/3/folders/add.php" "POST" url-request-data)))

;; (defun toodledo-reset () (oauth2-url-retrieve-synchronously (toodledo-token) "https://api.toodledo.com/3/account/get.php"))

(defun toodledo-notes-get (&optional options) "subject to change"
       (let* ((url "https://api.toodledo.com/3/notes/get.php")
              (args '()))
         (mapc (lambda (o)
                 (if (plist-get options o)
                     (setq args (cons (format "%s=%s" o (plist-get options o)) args))))
               (list 'before 'after 'id 'start 'num))
         (toodledo-api-request (if (seq-empty-p args) url (concat url "?" (mapconcat 'identity args "&"))))))

(defun toodledo-note-add (notes) "subject to change"
       (let* ((url "https://api.toodledo.com/3/notes/add.php")
              (stuff (json-encode notes))
              (url-request-data
               (mapconcat (lambda (arg)
                            (concat (url-hexify-string (car arg))
                                    "="
                                    (url-hexify-string (cdr arg)))) (list (cons "notes" stuff)) "&")))

         (toodledo-api-request url "POST" url-request-data)))

;; (toodledo-note-get ;; '(id "1483807285")
;;  );; '(after "1483664875"))

(defun init-notes ()
  ;; get notes buffer
  (save-excursion

    (let ((notes (toodledo-notes-get)))
      (mapc (lambda (note)
              (goto-char (point-max))
              (unless (looking-at "^")
                (insert "\n"))
              (insert "* ")
              (insert (plist-get note :title))
              (org-entry-put (point) "id" (number-to-string (plist-get note :id)))
              (org-entry-put (point) "added" (number-to-string (plist-get note :added)))
              (org-entry-put (point) "modified" (number-to-string (plist-get note :modified)))
              (let ((text (plist-get note :text) ))
                (if (> (length text) 0) (progn
                       (insert "\n")
                       (insert "** ")
                       (insert text))))
              (insert "\n")
              ) (cdr notes)))))
;; (toodledo-note-add (list '((title . "my first title"))
;;                          '((title . "my second title") (text . "more info about it"))))
(provide 'toodledo)
;;; toodledo ends here
